import React from 'react';

const Header = ({ title }) => 
        <div className="navbar">
            <p className="title">{title}</p>
            <nav></nav>
            <p>{}</p>
        </div>;

Header.defaultProps = {
    title: "The Wizard Manager"
};

export default Header;