import React from 'react';
import Header from './header';
import Footer from './footer';
import WizardList from './wizard/wizard-list';
import WizardForm from './wizard/wizard-add';

export default () => 
      <div>
        <Header title='The Wizard Manager'></Header>
        <WizardForm></WizardForm>
        <WizardList></WizardList>
        <Footer></Footer>
      </div>;