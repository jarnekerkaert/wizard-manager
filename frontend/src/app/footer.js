import React from "react";

const Footer = () => (
  <div className="container footer">
    <p>&#169; Jarne Kerkaert 2020</p>
  </div>
);

export default Footer;
