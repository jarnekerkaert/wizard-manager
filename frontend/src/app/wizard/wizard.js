import React from 'react';
import { connect } from 'react-redux';
import { castSpell } from '../redux/actions';

const Wizard = props =>
    <div className='container card'>
        <p className='title'>{props.wizard.name}</p>
        <p>Age: {props.wizard.age}</p>
        <button
            className='outline'
            onClick={() => props.castSpell(props.wizard.id)}
            disabled={props.wizard.cooldown > 0}
        >{props.wizard.cooldown > 0 ? props.wizard.cooldown : 'Cast spell'}</button>
    </div>;

export default connect(
    null, dispatch => ({castSpell: id => dispatch(castSpell(id))})
)(Wizard);