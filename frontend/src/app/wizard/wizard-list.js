import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchWizards } from '../redux/actions';
import Wizard from './wizard';

const WizardList = ({ wizards, fetchWizards }) => {
    useEffect(() => {
        fetchWizards();
    }, []);

    return (<div className="card-list">
        {wizards ? wizards.map((wizard) =>
            <Wizard
                wizard={wizard}
                key={wizard.id}
            ></Wizard>
        ) :
            <div>
                The wizards are on vacation
            </div>
        }
    </div>);
};

export default connect(
    state => {
        return { wizards: state.wizards }
    },
    dispatch => {
        return {
            fetchWizards: () => dispatch(fetchWizards()),
        };
    }
)(WizardList);