import React from 'react';
import { connect } from 'react-redux';
import { validateAddWizard } from '../redux/actions';

const WizardForm = ({ handleSubmit, errors }) =>
    <form className="container" onSubmit={handleSubmit}>
        <input id="name" placeholder="Name"></input>
        <input id="age" name="age" placeholder="Age" type="number"></input>

        <button
            type="submit"
            className="filled"
        >Add</button>

        {
            (errors && errors.length != 0) && errors.map(error => 
                <p className="error" key={error}>{error}</p>
            )
        }
    </form>;

export default connect(
    state => {
        return { errors: state.errors }
    },
    dispatch => {
        return {
            handleSubmit: wizard => {
                wizard.preventDefault();
                const { target: { name, age } } = wizard;
                dispatch(validateAddWizard({
                    name: name.value,
                    age: age.value
                }));
            }
        };
    }
)(WizardForm);