import { SET_ERRORS } from '../actiontypes';

export default (state = [], action) => {
    switch(action.type) {
        case SET_ERRORS: {
            return action.errors;
        }
        default: return state;
    }
}