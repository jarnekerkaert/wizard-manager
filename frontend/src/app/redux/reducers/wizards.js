import { CAST_SPELL, SET_WIZARDS, ADD_WIZARD } from '../actiontypes';

export default (state = [], action) => {
    switch (action.type) {
        case SET_WIZARDS: {
            return action.wizards;
        }
        case ADD_WIZARD: {
            return [...state,action.wizard];
        }
        case CAST_SPELL: {
            const newState = [...state];
            let newWizard = newState.find(wiz => wiz.id === action.wizardId);
            newWizard.cooldown = 10;
            newWizard.cooldownId = setInterval(() => console.log(newWizard.name + ' cast fireball!'), 1000);
            return newState;
        }
        default:
            return state;
    }
}