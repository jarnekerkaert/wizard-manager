import { CAST_SPELL, SET_WIZARDS, ADD_WIZARD, SET_ERRORS } from './actiontypes';
import { apiUrl } from '../../environment';
import axios from 'axios';

export const setWizards = wizards => ({
    type: SET_WIZARDS,
    wizards
});

export const castSpell = wizardId => ({
    type: CAST_SPELL,
    wizardId
});

export const addWizard = wizard => ({
    type: ADD_WIZARD,
    wizard
});

export const setErrors = errors => ({
    type: SET_ERRORS,
    errors
});

export const validateAddWizard = ({ name, age }) => dispatch => {
    var errors = [];

    if (name !== '' && !isNaN(age) && !(age <= 0)) {
        axios.post(`${apiUrl}/wizards`, {
            name: name,
            age: +age
        })
        .then(() => dispatch(fetchWizards()))
        .catch(error => console.error(error.message));
    }
    else {
        errors.push('Fields are required');
    }

    dispatch(setErrors(errors));
}

export const fetchWizards = () => dispatch => {
    axios.get(`${apiUrl}/wizards`)
        .then(result => dispatch(setWizards(result.data)))
        .catch(error => console.error(error.message));
}