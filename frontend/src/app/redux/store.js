import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import wizards from './reducers/wizards';
import errors from './reducers/errors';
import thunk from 'redux-thunk';

export default createStore(
    combineReducers({ wizards, errors }),
    compose(
        applyMiddleware(thunk),
        (window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
    )
);