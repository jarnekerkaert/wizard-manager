import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';

export default function ErrorSnackbar(props) {
  return (
    <div>
    <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={props.message != ''}
          autoHideDuration={6000}
          message={props.message}
        />
    </div>
  );
}