import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './app/redux/store';
import WizardApp from './app/wizardapp'
import 'normalize.css';
import './assets/default-style.scss'
import './assets/style.scss';

console.log("Initializing...");

render(
    <Provider store={store}>
        <WizardApp></WizardApp>
    </Provider>,
    document.getElementById("root")
);