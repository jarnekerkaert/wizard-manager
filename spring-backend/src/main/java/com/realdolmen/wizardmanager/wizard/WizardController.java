package com.realdolmen.wizardmanager.wizard;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/api/wizards")
@CrossOrigin("*")
@AllArgsConstructor
public class WizardController {
    private final WizardRepository repository;

    @GetMapping
    public Flux<Wizard> getAllWizards() {
        return repository.findAll();
    }

    @PostMapping
    public Mono<Wizard> addWizard(@RequestBody Wizard wizard) {
        return repository.save(wizard);
    }
}
