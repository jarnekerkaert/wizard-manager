package com.realdolmen.wizardmanager.wizard;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WizardRepository extends ReactiveMongoRepository<Wizard, String> {}
