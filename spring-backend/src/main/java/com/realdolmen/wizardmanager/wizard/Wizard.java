package com.realdolmen.wizardmanager.wizard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Wizard {
    @Id
    private String id;
    private String name;
    private Integer age;
    private Integer mana;
}
