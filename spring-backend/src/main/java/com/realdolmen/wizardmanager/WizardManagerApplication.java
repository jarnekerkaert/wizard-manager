package com.realdolmen.wizardmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WizardManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(WizardManagerApplication.class, args);
    }
}
